﻿using Products.Models;
using MediatR;
using System.Collections.Generic;

namespace Products.Commands
{
    public class AddCategoryCommand:IRequest<IEnumerable<EcomCategory>>
    {
        public EcomCategory CategoryName { get; set; }
    }
}
