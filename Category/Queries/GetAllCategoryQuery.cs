﻿using Products.Models;
using MediatR;
using System.Collections.Generic;

namespace Products.Queries
{
    public class GetAllCategoryQuery:IRequest<IEnumerable<EcomCategory>>
    {
    }
}
