﻿using Products.Models;
using MediatR;

namespace Products.Queries
{
    public class GetCategoryByIdQuery:IRequest<EcomCategory>
    {
        public int CategoryId { get; set; }
    }
}
