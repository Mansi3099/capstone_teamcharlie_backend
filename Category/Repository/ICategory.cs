﻿using Products.Models;
using System.Collections.Generic;

namespace Products.Repository
{
    public interface ICategory
    {
        EcomCategory getCategoryById(int id);
        IEnumerable<EcomCategory> GetAllCategory();

        public IEnumerable<EcomCategory> AddCategory(EcomCategory cat);



    }
}
