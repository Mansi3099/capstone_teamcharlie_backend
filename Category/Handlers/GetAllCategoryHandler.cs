﻿using Products.Models;
using Products.Queries;
using Products.Repository;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Products.Handlers
{
    public class GetAllCategoryHandler : IRequestHandler<GetAllCategoryQuery, IEnumerable<EcomCategory>>
    {
        private readonly ICategory _data;
        public GetAllCategoryHandler(ICategory data)
        {
            _data = data;
        }
        public async Task<IEnumerable<EcomCategory>> Handle(GetAllCategoryQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllCategory());
        }
    }
}
